# -*- coding: utf-8 -*-
import unittest
import os

def path_exists(abs_file_path, base, name):
    try:
        assert os.path.isfile(abs_file_path)
        return os.path.normcase(abs_file_path)
    except Exception as e:
        raise OSError('%s/%s no such file '%(base, name))


def file_exists(name="TESTEDOK.jpg", base="TestFiles"):
    script_dir = os.path.abspath(__file__).split("src")[0]
    abs_file_path = os.path.join(script_dir, base, name)
    return path_exists(abs_file_path, base, name)


class Fileexists(unittest.TestCase):
  
    def test_file_exists(self):
        assert file_exists()

    def test_file_exists_missing_name(self):
        assert file_exists(base="TestFiles")

    def test_file_exists_missing_base(self):
        assert file_exists(name="TESTEDOK.jpg")

    def test_file_exists_missing_file(self):
        self.assertRaises(OSError, file_exists, name="TESTEDOK55555.jpg")


if __name__ == "__main__":
    unittest.main(verbosity=2
    #~ defaultTest='Fileexists.test_driver_exists'
    )
