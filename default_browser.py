# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import os
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from file_name_path import file_exists

missed_default_browser = Exception("Please specify default browser in local_settings")


def default_browser(browser, binary = ''):
    """From local_settins.py choose browser&firefox_binary or raise error."""
    if browser == 'FF':
        if len(binary) > 10:
            binary = FirefoxBinary(binary)
            try:
                driver = webdriver.Firefox(firefox_binary=binary)
            except FileNotFoundError as e:
                raise e
        else:
            driver = webdriver.Firefox()
    elif browser == 'Chrome':
        script_dir = os.path.dirname(os.path.realpath(__file__))
        abs_file_path = os.path.join(script_dir,"selenium", "chromedriver")
        assert os.path.isfile(abs_file_path)
        driver_path = abs_file_path
        driver = webdriver.Chrome(executable_path=driver_path)
    else:
        raise missed_default_browser
    return driver
