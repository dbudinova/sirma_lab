# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import unittest
import datetime
__author__="d.budinova@gmail.com"

from class_generator_sirma import (TestGenerator,
                        attributes_set_to_class, make_function)


url_base = "https://{}/searchresults.{}.html"\
    "?aid=327345&label=wizz-footer&sid=9fae94de0766fbc31689168b77204c6c&" \
    "checkin_month=2&checkin_monthday=25&checkin_year=2018&checkout_month=3&"\
    "checkout_monthday=17&checkout_year=2018&city=-850553&class_interval=1&"\
    "dest_id=-850553&dest_type=city&dtdisc=0&from_sf=1&group_adults=12&group_children=0"\
    "&inac=0&index_postcard=0&label_click=undef&lang={}&no_rooms=1&offset=0&postcard=0&"\
    "room1=A%2CA%2CA%2CA%2CA%2CA%2CA%2CA%2CA%2CA%2CA%2CA&sb_price_type=total&soz=1&"\
    "src=searchresults&src_elem=sb&ss=%D0%91%D1%83%D0%B4%D0%B0%D0%BF%D0%B5%D1%89%D0%B0&"\
    "ss_all=0&ssb=empty&sshis=0&ssne=%D0%91%D1%83%D0%B4%D0%B0%D0%BF%D0%B5%D1%89%D0%B0&"\
    "ssne_untouched=%D0%91%D1%83%D0%B4%D0%B0%D0%BF%D0%B5%D1%89%D0%B0&"\
    "lang_click=other&cdl={}&selected_currency={}"
result_start = datetime.datetime(2018, 2, 25)
result_end = datetime.datetime(2018, 3, 17)


class TestBooking(TestGenerator):
    @classmethod
    def tearDownClass(self):
        "for debug purposes comment the row bellow"
        self.driver.quit()


def do_tests():
    """generate and execute tests for TestBooking:
        - test_booking_ru_ru
        - test_booking_en-gb
        - test_booking_bg 
    """
    website = "booking.com"
    #~ currency_unique = {'en-gb':'GBP', 'bg':('BGN','GBP'), 'ru':('RUB','BGN')}
    currency_unique = {'en-gb':'GBP', 'bg':'BGN', 'ru':'RUB'}
    test_unique = []
    url_list = []
    for lang in currency_unique.keys():
        if isinstance(currency_unique[lang], tuple):
            for c in currency_unique[lang]:
                url = url_base.format(website, lang, lang, lang, c)
                test_unique.append((url, lang, c))
        else:
            url = url_base.format(website, lang, lang, lang, currency_unique[lang])
            test_unique.append((url, lang, currency_unique[lang]))
        attributes_set_to_class(link=website, class_name=TestBooking, random=None,
                                                make_function=make_function, exception=None, 
                                                #~ make_function=make_function, exception='ru', 
                                                test_unique=test_unique, result_start=result_start,
                                                result_end=result_end)


if __name__ == "__main__":
    do_tests()
    unittest.main(verbosity=2)
