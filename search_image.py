#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""find the image_to_find inside the target_image, if found optionally mark a rectangle on markfile

# Example 1:
# search-image.py orig.png menu_hamburger.png

# Example 2:
# copy orig.png marked_result.png
# search-image.py orig.png menu_hamburger.png marked_result.png
"""

import sys, subprocess, cv2
from PIL import Image, ImageDraw

def search_image(target_image, image_to_find, result_image=None):
    #~ print('Load images and get dimensions - y,x')
    im = cv2.imread(target_image)
    tmp = cv2.imread(image_to_find)
    image_size = im.shape[:2]
    template_size = tmp.shape[:2]
    #print('Match Template')
    result = cv2.matchTemplate(im, tmp, cv2.TM_SQDIFF)
    #print('Get the Min Max Loc')
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    confidence = (9999999999 - min_val) / 100000000
    altconfidence = 100 - ((min_val / max_val)*100)
    topleftx = min_loc[0]
    toplefty = min_loc[1]
    sizex = template_size[1]
    sizey = template_size[0]
    if (altconfidence > 99) or ((confidence > 97) and (altconfidence > 93)) \
        or ((confidence > 95.7) and (altconfidence > 96.3)):
            print('The image of size', template_size, '(y,x) was found at', min_loc)
            if result_image:
                print('Marking', result_image, 'with a red rectangle')
                marked = Image.open(result_image)
                draw = ImageDraw.Draw(marked)
                draw.line(((topleftx, toplefty), (topleftx + sizex, toplefty)), fill="red", width=2)
                draw.line(((topleftx + sizex, toplefty), (topleftx + sizex, toplefty + sizey)),
                                    fill="red", width=2)
                draw.line(((topleftx + sizex, toplefty + sizey), (topleftx, toplefty + sizey)),
                                    fill="red", width=2)
                draw.line(((topleftx, toplefty + sizey), (topleftx, toplefty)), fill="red", width=2)
                del draw 
                marked.save(result_image, "PNG")
            return True
    else:
        print('The image was not found')
        return False

def main():
    if (len(sys.argv) > 2):
       print(sys.argv[1], sys.argv[2])
       target_image = sys.argv[1]
       image_to_find = sys.argv[2]
       search_image(target_image, image_to_find, result_image=None)
       sys.exit(0)
    else:
       print('Specify target image, followed by the image to search for.')
       print('Example: search-image.py target_image.png image_to_find.png [markfile.png]')
       sys.exit(1)


if __name__ == '__main__':
    main()
