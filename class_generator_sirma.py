#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import unittest
import os
import locale
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import (NoSuchElementException, TimeoutException)
from default_browser import default_browser
from search_image import search_image
import util

#~ browser = 'FF'
browser = 'Chrome'
#~ This will throw a TimeoutException whenever the page load takes more than 10 seconds.
set_page_load_timeout = 10

class TestGenerator(unittest.TestCase):
    """ BASE class in template for creating AT for an url
         GENERATOR SYNTETIC TESTS - NEEDS LINK"""

    @classmethod
    def setUpClass(self):
        """set browser and path for crossbrowser testing:
        example:
            self.driver = default_browser('FF', binary = firefox_binary)"""
        self.driver = default_browser(browser, binary ="")
        self.driver.implicitly_wait(15)
        self.driver.set_page_load_timeout(15)

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException: return False
        return True

    def is_alert_present(self):
        #~ try: self.driver.switch_to_alert()
        try: self.driver.switch_to.alert
        except NoAlertPresentException: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    @classmethod
    def tearDownClass(self):
        self.driver.quit()


def make_function(params=None):
    """function used internally to produce Python callable objects"""

    def common(self):
        """main function for L10n, i18n
        locale.LC_MONETARY, locale.LC_TIME, etc.
        """
        driver = self.driver
        url = params[0]
        try:
            self.assertIn('https', url)
        except (IndexError, KeyError) as e:
            raise e
        #~ driver.set_page_load_timeout(15)
        wait = WebDriverWait(driver, 10)
        try:
            driver.get(url)
        except TimeoutException as e:
            print('TimeoutException at url: %s'%url, e)
            raise e
        timestamp = datetime.isoformat(datetime.now()).split(".")[0]
        
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        screenshot = driver.save_screenshot('./screenshots/booking-orig-%s-%s.png' % (params[1], timestamp))
        base = "screenshots"
        script_dir = os.path.dirname(os.path.realpath(__file__))
        abs_file_path = os.path.join(script_dir, base, 'wizz.png')
        screenshot_file_path = os.path.join(script_dir, base, 'booking-orig-%s-%s.png' % (params[1], timestamp))
        self.assertTrue(os.path.isfile(screenshot_file_path))
        self.assertTrue(os.path.isfile(abs_file_path))
        self.assertTrue(search_image(screenshot_file_path, abs_file_path, screenshot_file_path))

        start = driver.find_element_by_class_name("sb-date-field__display")
        b_google_map_table = driver.find_element_by_id("b_google_map_thumbnail")
        price = driver.find_element_by_class_name("totalPrice")
        price_value = price.text.split(': ')[1]
        price_text = price.text.split(': ')[0]
        result_start = params[3]
        result_end = params[4]
        abs_file_path = os.path.join(script_dir, base, 'map-ru.png' )
        self.assertTrue(os.path.isfile(abs_file_path))
        "required API calls and responces for real automation"
        if params[1] == "bg" and ("BGN" in driver.current_url):
            locale.setlocale(locale.LC_ALL, 'bg_BG.UTF-8')
            self.assertIn("Цена за 12 възрастни за 20 нощувки ", price.text)
            self.assertIn("неделя, 25 февруари 2018 г.", start.text)
            self.assertIn(result_start.strftime("%A, %d %b").lower(), start.text.lower())
            self.assertIn("BGN ", price_value)
            self.assertNotIn(".", price_value)
            self.assertNotIn(",", price_value)
            self.assertIn("Цена за 12 възрастни за 20 нощувки", price_text)
            self.assertIn("Отворете картата", b_google_map_table.get_attribute('title'))
        elif params[1] == "ru" and  ("RUB" in driver.current_url):
            locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
            self.assertIn("Цена за 12 взрослых на 20 ночей:", price.text)
            self.assertIn("воскресенье, 25 февраля 2018", start.text)
            self.assertTrue(search_image(screenshot_file_path, abs_file_path, screenshot_file_path))
            self.assertIn("Цена за 12 взрослых на 20 ночей", price_text)
            self.assertTrue(price_value.endswith("руб."))
            self.assertIn("руб", price_value)
            self.assertNotIn(".", price_value.split('руб')[0])
            self.assertNotIn(",", price_value)
            self.assertIn(result_start.strftime("%A, %d %b").lower(), start.text.lower())
            self.assertIn("Открыть карту", b_google_map_table.get_attribute('title'))
        elif params[1] == "en-gb"  and ("GBP" in driver.current_url):
            locale.setlocale( locale.LC_ALL, 'en_GB.utf8')
            self.assertIn("Price for 12 adults for 20 nights:", price.text)
            self.assertIn("Sunday 25 February 2018", start.text)
            self.assertIn("£", price_value)
            self.assertTrue(price_value.startswith("£"))
            self.assertNotIn(".", price_value.split('£')[1])
            self.assertNotIn(" ", price_value.split('£')[1])
            self.assertIn(",", price_value)
            self.assertIn(result_start.strftime("%A %d %b").lower(), start.text.lower())
            self.assertIn("Open map", b_google_map_table.get_attribute('title'))
            self.assertFalse(search_image(screenshot_file_path, abs_file_path, screenshot_file_path))
        else:
            print("other")
        "save full screenshot with Chrome"
        util.fullpage_screenshot(self.driver, "./screenshots/test_booking-orig-%s-%s.png" % (params[1], timestamp))

    return common


class TestBooking(TestGenerator):
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
        #~ pass


def attributes_set_to_class(link="", class_name=TestBooking, random=None,
            make_function=make_function, exception = None, test_unique=None, 
            result_start="", result_end="", **kwd):
    """set attributes to class like *test_booking_ru*
        example: attributes_set_to_class(link="booking.com", class_name=TestBooking, 
                    random="ru", make_function=make_function, exception="bg")
        if os.environ.get('RANDOMIZE', 'Not Set') == "1" and if random="ru" - execute test for language "ru",
        if os.environ.get('RANDOMIZE', 'Not Set') == "0" and exception - execute test for lang "bg"
    """
    for url, lang, currency in test_unique:
        try:
            if exception and str(exception) != lang: 
                continue
        except Exception as e:
            print("attributes_set_to_class", e)
        if isinstance(currency, tuple):
            for c in currency:
                test_func = make_function(params=[url,  lang, c, result_start, result_end])
                setattr(class_name, 'test_{0}_{1}_{2}'.format(link[:-4], lang, c.lower()), test_func)
        else:
            test_func = make_function(params=[url,  lang, currency, result_start, result_end])
            setattr(class_name, 'test_{0}_{1}_{2}'.format(link[:-4], lang, currency.lower()), test_func)
    return class_name


if __name__ == "__main__":
    import doctest
    doctest.testmod()