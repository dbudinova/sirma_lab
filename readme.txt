*TestGenerator* - GENERATOR of SYNTETIC TESTS by given host and {'language1':'currency1', 'language2':(''currency1'',''currency2'),...}
__Generate new test just with adding new {'languageN':'currencyN'}__
--------------------------------------------------------------------------------

*example*: Linux setup
--------------------------------------------------------------------------------
mkdir sirma-lab
cd sirma-lab
virtualenv -p python3 sirmalab-env
source sirmalab-env/bin/activate
git clone https://{user}@bitbucket.org/dbudinova/sirma_lab.git
pip install -r requirements.txt

How do I add locale to ubuntu server?
-----------------------------------------------
check which locales are supported:

locale -a
add the locales you want (for example ru):

sudo locale-gen ru_RU
sudo locale-gen ru_RU.UTF-8
run this update comand

sudo update-locale 


*example*: run the tests for  {'en-gb':'GBP', 'bg':'BGN', 'ru':'RUB'}:
--------------------------------------------------------------------------------
python test_booking.py
...
Ran 3 tests in 42.367s

*example*: run the tests for {'en-gb':'GBP', 'bg':('BGN','GBP'), 'ru':('RUB','BGN')}
--------------------------------------------------------------------------------

test_booking_bg_bgn (__main__.TestBooking)
Marking ./sirma_lab/screenshots/booking-orig-bg-2018-02-22T23:36:06.png with a red rectangle
Finishing chrome full page screenshot workaround...
ok
test_booking_bg_gbp (__main__.TestBooking)
main function for L10n, i18n ... The image of size (102, 193) (y,x) was found at (389, 153)
Marking ./sirma_lab/screenshots/booking-orig-bg-2018-02-22T23:36:21.png with a red rectangle
ok
test_booking_en-gb_gbp (__main__.TestBooking)
Marking /home/dani/sp2016/sirma-lab/sirma_lab/screenshots/booking-orig-en-gb-2018-02-22T23:36:35.png with a red rectangle
FAIL
test_booking_ru_bgn (__main__.TestBooking)
Marking ./sirma_lab/screenshots/booking-orig-ru-2018-02-22T23:36:41.png with a red rectangle
ok
test_booking_ru_rub (__main__.TestBooking)
Marking ./sirma_lab/screenshots/booking-orig-ru-2018-02-22T23:36:54.png with a red rectangle
ok

======================================================================
FAIL: test_booking_en-gb_gbp (__main__.TestBooking)
main function for L10n, i18n
----------------------------------------------------------------------
Traceback (most recent call last):
  File "./sirma_lab/class_generator_sirma.py", line 135, in common
    self.assertFalse(search_image(screenshot_file_path, abs_file_path, screenshot_file_path))
AssertionError: True is not false

----------------------------------------------------------------------
Ran 5 tests in 65.206s
_one test failed because Visual analysis is not with maximum confidence._


*example*: run only tests for one language:
--------------------------------------------------------------------------------
  in  test_booking.py - do_tests() set *attributes_set_to_class exception='ru'*

*NB: required an implementation of API requests for full automation*
--------------------------------------------------------------------------------

*example*: save full screenshot with Chrome, adapted https://stackoverflow.com/questions/41721734/taking-screenshot-of-full-page-with-selenium-python-chromedriver
--------------------------------------------------------------------------------
import util
util.fullpage_screenshot(self.driver, "path_to_file.png")


*example*: adapted https://github.com/Qarj/search-image
--------------------------------------------------------------------------------
from search_image import search_image
search_image(screenshot_file_path, abs_file_path, screenshot_file_path)